$(document).ready(function() {
  $("#form-user").validate({
    rules: {
      username: "required",
      password: "required",
      privilage: "required",
    },
    messages:   {
      username: { required: 'This fill is required' },
      password: { required: 'This fill is required' },
      privilage: { required: 'This fill is required' }
    }
  });

});



$(document).ready(function() {
  $.validator.addMethod("alpha", function(value, element) {
   return this.optional(element) || value == value.match(/^[.,!()a-zA-Z" "]+$/);
  },"Text Only");

  $.validator.addMethod("nmbr", function(value, element) {
  return this.optional(element) || value == value.match(/^[.0-9-+()" "]+$/);
  },"Number and special character only");

  $.validator.addMethod("latt", function(value, element) {
  return this.optional(element) || value == value.match(/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)/);
  },"Wrong format latitude");


  $("#form-provider").validate({
    rules: {
      nmrumahsakit: {required: true,alpha: true},
      alamat:       {required: true,alpha: true},
      kota:         {required: true,alpha: true},
      provinsi:     {required: true,alpha: true},
      rawat_inap:   {required: true,alpha: true},
      rawat_jalan:  {required: true,alpha: true},
      mcu:          {required: true,alpha: true},
      telp:         {required: true,nmbr: true},
      fax:          {required: true,nmbr: true},
      keterangan:   {required: true,alpha: true},
      latitude:     {required: true,nmbr: true},
      longitude:    {required: true,nmbr: true}
    },
    messages: {
      required: "This fill is required"
    }
  });
});



$(document).ready(function() {
  $("#form-proinfo").validate({
    rules: {
      namafile: "required",
      konten: "required",
      foto: "required"
    },
    messages: {
      namafile: { required: 'This fill is required' },
      konten: { required: 'This fill is required' },
      foto: { required: 'This fill is required' }
    }
  });
});



$(document).ready(function() {
  $("#form-procinfo").validate({
    rules: {
      namafile: "required",
      konten: "required",
      foto: "required"
    },
    messages: {
      namafile: { required: 'This fill is required' },
      konten: { required: 'This fill is required' },
      foto: { required: 'This fill is required' }
    }
  });
});



$(document).ready(function() {
  $("#form-typenab").validate({
    rules: {
      nabtype: "required"
    },
    messages: {
      nabtype: { required: 'This fill is required' }
    }
  });
});



$(document).ready(function() {
  $("#form-datanab").validate({
    rules: {
      tgl_nab: "required"
    },
    messages: {
      tgl_nab: { required: 'This fill is required' }
    }
  });
});



$(document).ready(function() {
  $("#form-video").validate({
    rules: {
      filename: "required",
      keterangan: "required",
      video: "required"
    },
    messages: {
      filename: { required: 'This fill is required' },
      keterangan: { required: 'This fill is required' },
      video: { required: 'This fill is required' }
    }
  });
});



$(document).ready(function() {
  $("#form-slideshow").validate({
    rules: {
      filename: "required",
      deskripsi: "required",
      foto: "required"
    },
    messages: {
      filename: { required: 'This fill is required' },
      deskripsi: { required: 'This fill is required' },
      foto: { required: 'This fill is required' }
    }
  });
});
