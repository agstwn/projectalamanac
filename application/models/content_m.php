<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class content_m extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function get_allcontent()
  {
    $query = $this->db->get('tablecontent');
    return $query->result();
  }

  public function get_allcontent2()
  {
    $query = $this->db->get('tablecontent');
    return $query;
  }

  public function get_viewmore()
  {
    $query = $this->db->get('tablecontent');
    return $query;
  }

  public function get_allcontentstartandlimit($num,$start)
  {
    $query = $this->db->get('tablecontent',$num, $start);
    return $query->result();
  }

  public function get_allcontentlimit()
  {
    $this->db->order_by('id', "DESC");
    $query = $this->db->get('tablecontent',6);
    return $query->result();
  }

  public function get_allcontentech()
  {
    $this->db->order_by('id', "DESC");
    $this->db->where(" category LIKE '%tech%'");
    $query = $this->db->get('tablecontent',4);
    return $query->result();
  }

  public function countcontentbytype($type)
  {
    $this->db->order_by('id', "DESC");
    $this->db->where(" category LIKE '%$type%'");
    $query = $this->db->get('tablecontent');
    return $query;
  }

  public function get_contentbytype($num,$start,$type)
  {
    $this->db->order_by('id', "DESC");
    $this->db->limit($num,$start);
    $this->db->where('category', $type);
    $this->db->select('*');
    $this->db->from('tablecontent');
    $query = $this->db->get();
    return $query->result();
  }

  public function get_allcontentlifestyle()
  {
    $this->db->order_by('id', "DESC");
    $this->db->where(" category LIKE '%lifestyle%'");
    $query = $this->db->get('tablecontent',4);
    return $query->result();
  }

  public function get_recentcontent()
  {
    $query = $this->db->get('tablecontent',6);
    return $query->result();
  }

  public function get_categories()
  {
    $query = $this->db->get('tablecategory');
    return $query->result();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('tablecontent')->row();
  }

  public function add_content($data)
  {
    $this->db->insert('tablecontent', $data);
    return $this->db->insert_id();
  }

  public function deletingcontent($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tablecontent');
  }

  public function update_productinfo($id, $data)
  {
    $this->db->where('id', $id)->update('tablecontent', $data);
    return $this->db->affected_rows();
  }

  public function get_dataproinfo($where = NULL)
  {
  if (!is_null($where))
  {
    $this->db->where($where);
    $query = $this->db->get('tablecontent');

    if ($query->num_rows() > 0)
    {
      return $query->result();
    }
  }
  return FALSE;
}

}
/** Enf of PHP **/
