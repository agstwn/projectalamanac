<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class about_m extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function getallcontentbackend()
  {
    $query = $this->db->get('tableabout');
    return $query->result();
  }

  public function getallcontentfrontend()
  {
    $query = $this->db->get('tableabout');
    return $query->result_array();
  }

  public function add_about($data)
  {
    $this->db->insert('tableabout', $data);
    return $this->db->insert_id();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('tableabout')->row();
  }

  public function deletingabout($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tableabout');
  }

  public function update_about($id, $data)
  {
    $this->db->where('id', $id)->update('tableabout', $data);
    return $this->db->affected_rows();
  }

}
/** Enf of PHP **/
