<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class category_m extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function get_allcategory()
  {
    $query = $this->db->get('tablecategory');
    return $query->result();
  }

  public function add_category($data)
  {
    $this->db->insert('tablecategory', $data);
    return $this->db->insert_id();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('tablecategory')->row();
  }

  public function del_category($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tablecategory');
  }

  public function update_category($id, $data)
  {
    $this->db->where('id', $id)->update('tablecategory', $data);
    return $this->db->affected_rows();
  }

}
/** Enf of PHP **/
