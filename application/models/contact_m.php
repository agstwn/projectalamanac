<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contact_m extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function getallcontactbackend()
  {
    $query = $this->db->get('tablecontact');
    return $query->result();
  }

  public function getallcontactfrontend()
  {
    $query = $this->db->get('tablecontact');
    return $query->result_array();
  }

  public function add_contact($data)
  {
    $this->db->insert('tablecontact', $data);
    return $this->db->insert_id();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('tablecontact')->row();
  }

  public function deletingcontact($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tablecontact');
  }

  public function update_contact($id, $data)
  {
    $this->db->where('id', $id)->update('tablecontact', $data);
    return $this->db->affected_rows();
  }

}
/** Enf of PHP **/
