<?php

class Users_m extends CI_Model {

  public function __construct()
  {
          parent::__construct();
  }

  public function get_alladmin()
  {
    $query = $this->db->get('tableadmin');
    return $query->result();
  }

  /* Get all data konsumen */
  public function get_allkonsumen()
  {
    $query = $this->db->get('tb_konsumen');
    return $query->result();
  }

  /* Get by ID data admin */
  public function get_adminby($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('tableadmin')->row();
  }

  /* Get by ID data user */
  public function get_userby($id)
  {
    $this->db->where('kd_konsumen', $id);
    return $this->db->get('tb_konsumen')->row();
  }

  public function add_usermanage($data)
  {
    $this->db->insert('tableadmin', $data);
    return $this->db->insert_id();
  }

  public function update_admin($id, $data)
  {
    $this->db->where('id', $id)->update('tableadmin', $data);
    return $this->db->affected_rows();
  }

  public function del_adminby($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tableadmin');
  }

  public function del_userby($id)
  {
    $this->db->where('kd_konsumen', $id);
    $this->db->delete('tb_konsumen');
  }

  /*
  * ==========================
  * For Contact Us
  * ==========================
  */

  public function contactus($data)
  {
    $this->db->insert('tb_konsumen', $data);
    return $this->db->insert_id();
  }

}
/** Enf of PHP **/
