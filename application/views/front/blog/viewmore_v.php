<div class="row">
	<div class ="col-md-8">
		<div class="content-article">
			<div class="row">
				<?php
					foreach($view as $baris){
				?>
				<div class="col-md-10">
					<div class="service-container">
							<div class ="service-icon-modif">
								<img src="<?php echo base_url().'uploads/origin/content/'.$baris->img; ?>" class="img-responsive" alt="blog masonry last sunday">
								<span class="blog-detail-meta"><?php echo date("d F Y",strtotime($baris->created_at)); ?></span>
							</div>
							<div class ="service-content">
								<h4><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo $baris->judul; ?></a></h4>
								<p><?php echo substr($baris->isi,0,130)."..."; ?>
								</p>
							</div>
					</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class ="col-md-12">
			<ul class="pagination">
				<?php echo $halaman;?>
			</ul>
		</div>
		<!-- end:blog-single -->
	</div>
	<!-- end:article -->
	<!-- begin:sidebar -->
	<div class ="col-md-4">
			<!-- begin:widget-sidebar -->
			<div class ="content-article">
				<div class ="widget-sidebar">
						<div class  ="widget-title">
							<h3>Search</h3>
						</div>
						<div class  ="input-group">
							<input type="text" class="form-control" placeholder="Type your keyword">
							<span class="input-group-btn">
							<button class="btn btn-secondary" type="button"><i class="fa fa-arrow-right"></i></button>
							</span>
						</div>
				</div>
				<!-- break -->
				<div class ="widget-sidebar widget_recent_entries">
						<div class  ="widget-title">
							<h3>Most Popular</h3>
						</div>
						<ul>
							<?php
								foreach($recentcontent as $baris){
							?>
							<li><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo substr($baris->judul,0,50); ?></a></li>
							<?php } ?>
						</ul>
				</div>
				<div class ="widget-sidebar widget_recent_entries">
						<div class  ="widget-title">
							<h3>Recent Posts</h3>
						</div>
						<ul>
							<?php
								foreach($recentcontent as $baris){
							?>
							<li><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo substr($baris->judul,0,50); ?></a></li>
							<?php } ?>
						</ul>
				</div>
			</div>
			<!-- end:widget-sidebar -->
	</div>
	<!-- end:sidebar -->
</div>
