
	<div class="row">
		<!-- begin:article -->
		<div class="col-md-8">
			<!-- begin:blog-single -->
			<div class="content-article">
				<div class="row">
					<div class="col-md-12">
						<div class="page-title">
							<h2><?php echo $konten->judul; ?></h2>
							<span class="blog-detail-meta">By <a href="#"><a href="#"><?php echo $konten->createdBy; ?></a>
								<?php echo date("d/M/Y",strtotime($konten->created_at)); ?> In
								<a href="<?php echo base_url(); ?>page/categories/<?php echo $konten->category; ?>"><?php echo $konten->category; ?></a></span>
						</div>
						<img  src="<?php echo base_url().'uploads/origin/content/'.$konten->img; ?>" class="img-responsive img-blog-single" alt=""/>
						<p><?php echo $konten->isi; ?></p>
						<!-- begin:blog-post-sharing -->
						<div class="blog-post-sharing">
							<div class="row">
								<div class="col-sm-5 col-xs-5">
									<h4>Share this post :</h4>
								</div>

								<div class="col-sm-7 col-xs-7">
									<ul class="share-list">
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-tumblr"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- end:blog-post-sharing -->

					</div>
				</div>

				<!-- begin:blog-post-author -->
				<!-- <div class="row">
					<div class="col-md-12">
						<div class="blog-post-author">
							<div class="blog-post-author-img">
								<img src="img/author-avatar.jpg" alt="author avatar blog last sunday">
							</div>
							<div class="blog-post-author-desc">
								<h3>John Doe <small>- CEO Twinslabs Inc.</small></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.</p>
								<strong>Follow me at:</strong>
								<ul class="social-icon">
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div> -->
				<!-- end:blog-post-author -->

				<!-- begin:blog-post-related -->
				<!-- <div class="row">
					<div class="col-md-12">
						<h3 class="blog-single-title">Related Posts</h3>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="blog-container">
							<div class="blog-image">
								<img src="img/image-blog-post-thumbnail-01.jpg" class="img-responsive" alt="blog masonry last sunday">

								<div class="blog-author"><img src="img/author-avatar.jpg" alt="author avatar last sunday"></div>
								<div class="blog-content">
									<span class="label label-secondary">Walking</span>
									<h2><a href="blog-single.html">Lorem ipsum dolor sit amet consectetur</a></h2>
									<ul class="blog-meta">
										<li>1 hrs ago</li>
										<li>2k love</li>
										<li>100 comments</li>
									</ul>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="blog-container">
							<div class="blog-image">
								<img src="img/image-blog-post-thumbnail-02.jpg" class="img-responsive" alt="blog masonry last sunday">

								<div class="blog-author"><img src="img/avatar-comment.jpg" alt="author avatar last sunday"></div>
								<div class="blog-content">
									<span class="label label-primary">Travelling</span>
									<h2><a href="blog-single.html">Lorem ipsum dolor sit amet consectetur adisplicing</a></h2>
									<ul class="blog-meta">
										<li>1 hrs ago</li>
										<li>2k love</li>
										<li>100 comments</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div> -->



				<!-- begin:comment-form -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="blog-single-title">Leave a Comment</h3>
					</div>
				</div>
				<!-- end:comment-form -->

			</div>
			<!-- end:blog-single -->
		</div>
		<!-- end:article -->

		<!-- begin:sidebar -->
		<div class ="col-md-4">
				<!-- begin:widget-sidebar -->
				<div class ="content-article">
					<div class ="widget-sidebar">
							<div class  ="widget-title">
								<h3>Search</h3>
							</div>
							<div class  ="input-group">
								<input type="text" class="form-control" placeholder="Type your keyword">
								<span class="input-group-btn">
								<button class="btn btn-secondary" type="button"><i class="fa fa-arrow-right"></i></button>
								</span>
							</div>
					</div>
					<!-- break -->
					<div class ="widget-sidebar widget_recent_entries">
							<div class  ="widget-title">
								<h3>Most Popular</h3>
							</div>
							<ul>
								<?php
									foreach($recentcontent as $baris){
								?>
								<li><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo substr($baris->judul,0,50); ?></a></li>
								<?php } ?>
							</ul>
					</div>
					<div class ="widget-sidebar widget_recent_entries">
							<div class  ="widget-title">
								<h3>Recent Posts</h3>
							</div>
							<ul>
								<?php
									foreach($recentcontent as $baris){
								?>
								<li><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo substr($baris->judul,0,50); ?></a></li>
								<?php } ?>
							</ul>
					</div>
				</div>
				<!-- end:widget-sidebar -->
		</div>
		<!-- end:sidebar -->
	</div>
	<!-- end:main-section -->
