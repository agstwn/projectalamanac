<div class="row">
	<div class="col-md-12">
		<div class="content-article">
			<div class="heading-title">
				<h2>Contact</h2>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="content-article">
			<div class="row">
				<div class="col-md-8 col-sm-7 col-xs-12">
					<div width="400px" height="400px"><?php echo $kontak[0]['googlemap']; ?></div>
				</div>
				<div class="col-md-4 col-sm-5 col-xs-12">
					<form role="form">
						<div class="row">
							<li class="col-sm-6">
								<address>
									<h5>Head Office</h5>
									<p><?php echo $kontak[0]['alamat']; ?></p>
									<p><?php echo $kontak[0]['hp']; ?><br></p>
									<p><?php echo $kontak[0]['telp']; ?><br></p>
									<p><?php echo $kontak[0]['email']; ?><br></p>
								</address>
							</li>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
