<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="last sunday personal blog theme">
	<meta name="keywords" content="bootstrap, responsive, blog, portfolio, personal blog, masonry, isotope">
	<meta name="author" content="templateninja">
	<link rel="shortcut icon" href="img/favicon.png">
	<title>Last Sunday</title>
	<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/front/css/magnific-popup.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front/css/style.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front/css/responsive.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front/css/demo.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
</head>

<body id="page-top">
	<!-- begin:container -->
	<div class="container content-wrapper">
		<!-- begin:navbar -->
		<div class="content-article">
			<div class="row">
				<div class="col-md-12">
					<!-- navbar navbar-default navbar-fixed-top -->
					<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a class="navbar-brand" href="index.html"><img src="img/logo.png" alt=""></a>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right">
									<li><a href="<?php echo base_url(); ?>home">Home</a></li>
									<li><a href="<?php echo base_url(); ?>about">About</a></li>
									<li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<?php
												foreach($categories as $baris){
											?>
											<li><a href="<?php echo base_url(); ?>page/categories/<?php echo $baris->namaCategory; ?>"><?php echo $baris->namaCategory; ?></a></li>
											<?php }
											?>
										</ul>
									</li>
								</ul>
							</div>
							<!-- /.navbar-collapse -->
						</div>
						<!-- /.container-fluid -->
					</nav>
				</div>
			</div>
