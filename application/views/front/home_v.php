<div class="row">
	<div class="col-md-12">
		<div class="content-article">
			<div class="heading-title">
				<h2>Latest News</h2>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row container-blog">
			<?php
				foreach($homecontact as $baris){
			?>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="portfolio-container">
					<div class="portfolio-image">
						<img src="<?php echo base_url().'uploads/origin/content/'.$baris->img; ?>" class="img-responsive" alt="">
						<div class="portfolio-content">
							<span class="label label-secondary"><?php echo $baris->category; ?></span>
							<h2><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo $baris->judul; ?></a></h2>
						</div>
					</div>
				</div>
			</div>
			<?php
				}
			?>
		</div>
	<a href="<?php echo base_url(); ?>blog/viewmore">VIEW MORE</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="content-article">
			<div class="heading-title">
				<h2>Categories</h2>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="label-title text-center">
			<h3>Technology</h3>
		</div>
	</div>
	<?php
		foreach($contentech as $baris){
	?>
	<div class="col-md-6">
		<div class="service-container">
			<div class="service-icon-modif">
				<img src="<?php echo base_url().'uploads/origin/content/'.$baris->img; ?>" class="img-responsive" alt="blog masonry last sunday">
				<span class="spancolour"><?php echo date("d F Y",strtotime($baris->created_at)); ?></span>
			</div>
			<div class="service-content">
				<h4><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo $baris->judul; ?></a></h4>
				<p><?php echo substr($baris->isi,0,150)."..."; ?></p>
			</div>
		</div>
	</div>
	<?php
		}
	?>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="label-title text-center">
			<h3>Lifestyle</h3>
		</div>
	</div>
	<?php
		foreach($lifestyle as $baris){
	?>
	<div class="col-md-6">
		<div class="service-container">
			<div class="service-icon-modif">
				<img src="<?php echo base_url().'uploads/origin/content/'.$baris->img; ?>" class="img-responsive" alt="blog masonry last sunday">
				<span class="spancolour"><?php echo date("d F Y",strtotime($baris->created_at)); ?></span>
			</div>
			<div class="service-content">
				<h4><a href="<?php echo base_url(); ?>page/read/<?php echo $baris->id; ?>"><?php echo $baris->judul; ?></a></h4>
				<p><?php echo substr($baris->isi,0,150)."..."; ?></p>

			</div>
		</div>
	</div>
	<?php
		}
	?>
</div>
