</div>
</div>
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
				<div class="widget-footer">
					<ul class="social-icon">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						<li><a href="#"><i class="fa fa-soundcloud"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 copyright-left">
				<p class="footercopy">Copyright &copy; <?php echo date("Y") ?> All Right Reserved.</p>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/front/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script> -->
<script src="<?php echo base_url(); ?>assets/front/js/gmap3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/jquery.easing.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/jquery.backstretch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/masonry.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/magnificpopup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/demo.js"></script>
</body>

</html>
