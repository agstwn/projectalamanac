<section class="content-header">
  <h1>
    Content List
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>Content Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">List</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?php echo base_url();?>content/contentaddview" class="btn btn-primary" role="button">ADD DATA</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="ajax-loader">
            <center>
              <img src="<?php echo base_url(); ?>assets/img/loader.gif">
            </center>
          </div>
          <table id="datatable" class="table table-bordered table-striped" style="display: none">
            <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Images</th>
                <th>Videos</th>
                <th>Like</th>
                <th>Category</th>
                <th>Writer</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($content as $baris){
              ?>
              <tr>
                <td><?php echo $baris->id; ?></td>
                <td><?php echo $baris->judul; ?></td>
                <td><?php $content = $baris->isi;
                if($content == TRUE){
                  echo "Ada Konten";
                } else {
                  echo "Tidak Ada Konten";
                } ?></td>
                <td><?php echo $baris->img; ?></td>
                <td><?php echo $baris->video; ?></td>
                <td><?php echo $baris->fblike; ?></td>
                <td><?php echo $baris->category; ?></td>
                <td><?php echo $baris->createdBy; ?></td>
                <td><?php echo $baris->created_at; ?></td>
                <td><?php echo $baris->updated_at; ?></td>
                <td><a href="<?php echo base_url();?>content/vieweditcontent/<?php echo $baris->id; ?>" class="fa fa-edit">|
                    <a href="<?php echo base_url();?>content/delcontent/<?php echo $baris->id; ?>" class="fa fa-trash-o" onclick="return confirm('Apakah anda yakin?')">|
                    <a href="<?php echo base_url();?>content/viewcontent/<?php echo $baris->id; ?>" class="fa fa-eye"></a></td>
              </tr>
              <?php
                }
              ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
