<section class="content-header">
  <h1>
    Content Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i> Content Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Edit Content  Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form-proinfo" action="<?php echo site_url('content/editcontent/'.$editkonten->id); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Product</label>
              <input type="text" name="judul" id="judul" class="form-control"  value="<?php echo $editkonten->judul; ?>">
            </div>
            <div class='form-group'>
              <textarea id="editor1" name="isi" rows="10" cols="80">
                <?php echo $editkonten->isi; ?>
              </textarea>
            </div>
            <div class="form-group">
              <label>Category</label>
              <select class="form-control " name="category" id="" required>
                  <option value="<?php echo $editkonten->category; ?>"><?php echo $editkonten->category; ?></option>
                  <?php foreach ($category as $row) { ?>
                  <option value="<?php echo $row->namaCategory; ?>"><?php echo $row->namaCategory; ?></option>
                  <?php }?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Video</label>
              <input type="text" name="video" id="video" class="form-control" placeholder="<?php echo $editkonten->video; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">New Image</label>
              <input type="file" id="img" name="foto2" value="<?php echo $editkonten->img; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Old Image</label><br>
              <input type="text" id="img" name="images" value="<?php echo $editkonten->img; ?>">
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $editkonten->id; ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>content" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
