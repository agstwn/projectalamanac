<section class="content-header">
  <h1>
    About Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>About Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Add</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Add About Information</h3>
        </div>
        <form role="form" id="form-proinfo" action="<?php echo site_url('about/addingabout'); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul">
            </div>
            <div class='form-group'>
              <textarea id="editor1" name="isi" rows="10" cols="80">
                This fill is required
              </textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Image Upload</label>
              <input type="file" id="img" name="img">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>about/aboutbackend" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
