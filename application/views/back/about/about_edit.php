<section class="content-header">
  <h1>
    About Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i> About Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Edit About  Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form-proinfo" action="<?php echo site_url('about/editabout/'.$editkonten->id); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control"  value="<?php echo $editkonten->judul; ?>">
            </div>
            <div class='form-group'>
              <textarea id="editor1" name="isi" rows="10" cols="80">
                <?php echo $editkonten->isi; ?>
              </textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">New Image</label>
              <input type="file" id="img" name="foto2" value="<?php echo $editkonten->img; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Old Image</label><br>
              <input type="text" id="img" name="images" value="<?php echo $editkonten->img; ?>">
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $editkonten->id; ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>about/aboutbackend" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
