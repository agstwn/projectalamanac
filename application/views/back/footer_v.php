</div>
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; <?php echo date('Y'); ?> .</strong> All rights reserved.
</footer>
</div>


</body>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/autoNumeric.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>

<script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>

<script type="text/javascript">
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
  });

  $(function () {
  $('#datatable').dataTable({
    "fnDrawCallback": function( oSettings ) {
      $(".ajax-loader").css("display","none");
      $("#datatable").css("display","block");
      }
    });
  });

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    // $(".textarea").wysihtml5();
  });

  // Auto Numeric
  $(function () {
    $('.numero').autoNumeric('init');
  });

</script>
</html>
