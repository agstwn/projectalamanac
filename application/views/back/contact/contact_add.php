<section class="content-header">
  <h1>
    Contact Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>Contact Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Add</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Add Contact Information</h3>
        </div>
        <form role="form" id="form-proinfo" action="<?php echo site_url('contact/addingcontact'); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul">
            </div>
            <div class='form-group'>
              <textarea class="form-control" name="alamat" rows="3" placeholder="Alamat ..."></textarea>
            </div>
            <div class='form-group'>
              <textarea class="form-control" name="googlemap" rows="3" placeholder="Google Map ..."></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">HP</label>
              <input type="text" name="hp" id="judul" class="form-control" placeholder="HP">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telp</label>
              <input type="text" name="telp" id="judul" class="form-control" placeholder="Telp">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="text" name="email" id="judul" class="form-control" placeholder="Email">
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>contact/contactaddview" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
