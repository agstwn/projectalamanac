<section class="content-header">
  <h1>
    Contact List
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>Contact Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">List</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?php echo base_url();?>contact/contactaddview" class="btn btn-primary" role="button">ADD DATA</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="ajax-loader">
            <center>
              <img src="<?php echo base_url(); ?>assets/img/loader.gif">
            </center>
          </div>
          <table id="datatable" class="table table-bordered table-striped" style="display: none">
            <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>alamat</th>
                <th>Google Map</th>
                <th>hp</th>
                <th>Telp</th>
                <th>Email</th>
                <th>CreatedBy</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($contact as $baris){
              ?>
              <tr>
                <td><?php echo $baris->id; ?></td>
                <td><?php echo $baris->judul; ?></td>
                <td><?php echo $baris->alamat; ?></td>
                <td><?php $content = $baris->googlemap;
                if($content == TRUE){
                  echo "Ada Map";
                } else {
                  echo "Tidak Ada Map";
                } ?></td>
                <td><?php echo $baris->hp; ?></td>
                <td><?php echo $baris->telp; ?></td>
                <td><?php echo $baris->email; ?></td>
                <td><?php echo $baris->createdBy; ?></td>
                <td><?php echo $baris->created_at; ?></td>
                <td><?php echo $baris->updated_at; ?></td>
                <td><a href="<?php echo base_url();?>contact/vieweditcontact/<?php echo $baris->id; ?>" class="fa fa-edit">|
                    <a href="<?php echo base_url();?>contact/delcontact/<?php echo $baris->id; ?>" class="fa fa-trash-o" onclick="return confirm('Apakah anda yakin?')">|
                    <a href="<?php echo base_url();?>contact/viewcontact/<?php echo $baris->id; ?>" class="fa fa-eye"></a></td>
              </tr>
              <?php
                }
              ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
