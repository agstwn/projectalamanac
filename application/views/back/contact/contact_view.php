<section class="content-header">
  <h1>
    Contact Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i> Contact Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">View</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">View Contact Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Category</label>
              <?php echo $kontak->judul; ?>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Alamat</label>
              <?php echo $kontak->alamat; ?>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Google Map</label>
              <?php echo $kontak->googlemap; ?>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">HP</label>
                <?php echo $kontak->hp; ?>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Telp</label>
                <?php echo $kontak->telp; ?>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <?php echo $kontak->email; ?>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Created By</label>
                <?php echo $kontak->createdBy; ?>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url()?>contact/contactbackend" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
