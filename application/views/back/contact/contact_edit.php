<section class="content-header">
  <h1>
    Contact Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i> Contact Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Edit Contact  Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form-proinfo" action="<?php echo site_url('contact/editcontact/'.$editkontak->id); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control" value="<?php echo $editkontak->judul; ?>">
            </div>
            <div class='form-group'>
              <textarea class="form-control" name="alamat" rows="3" value="<?php echo $editkontak->alamat; ?> ..."></textarea>
            </div>
            <div class='form-group'>
              <textarea class="form-control" name="googlemap" rows="3" value="Google Maps..."></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">HP</label>
              <input type="text" name="hp" id="judul" class="form-control" value="<?php echo $editkontak->hp; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telp</label>
              <input type="text" name="telp" id="judul" class="form-control" value="<?php echo $editkontak->telp; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="text" name="email" id="judul" class="form-control" value="<?php echo $editkontak->email; ?>">
            </div>
          </div>
          <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $editkontak->id; ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>contact/contactbackend" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
