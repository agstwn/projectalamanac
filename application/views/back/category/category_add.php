<section class="content-header">
  <h1>
    Category Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>Category Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Add</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Add Category Information</h3>
        </div>
        <form role="form" id="form-proinfo" action="<?php echo site_url('category/addingcategory'); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" name="namaCategory" id="namaCategory" class="form-control" placeholder="Nama Category">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Image Upload</label>
              <input type="file" id="img" name="imageCategory">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>category" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
