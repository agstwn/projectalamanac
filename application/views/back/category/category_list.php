<section class="content-header">
  <h1>
    Category List
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i>Category Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">List</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?php echo base_url();?>category/contentaddview" class="btn btn-primary" role="button">ADD DATA</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="ajax-loader">
            <center>
              <img src="<?php echo base_url(); ?>assets/img/loader.gif">
            </center>
          </div>
          <table id="datatable" class="table table-bordered table-striped" style="display: none">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Category</th>
                <th>Created By</th>
                <th>Images</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($content as $baris){
              ?>
              <tr>
                <td><?php echo $baris->id; ?></td>
                <td><?php echo $baris->namaCategory; ?></td>
                <td><?php echo $baris->createdBy; ?></td>
                <td><?php echo $baris->imageCategory; ?></td>
                <td><?php echo $baris->created_at; ?></td>
                <td><?php echo $baris->updated_at; ?></td>
                <td><a href="<?php echo base_url();?>category/form_editcategory/<?php echo $baris->id; ?>" class="fa fa-edit">|
                    <a href="<?php echo base_url();?>category/del_productinfo/<?php echo $baris->id; ?>" class="fa fa-trash-o" onclick="return confirm('Apakah anda yakin?')">|
                    <a href="<?php echo base_url();?>category/viewcategory/<?php echo $baris->id; ?>" class="fa fa-eye"></a></td>
              </tr>
              <?php
                }
              ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
