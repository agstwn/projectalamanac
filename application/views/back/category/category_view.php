<section class="content-header">
  <h1>
    Category Information
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-book"></i> Category Information</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">View</li>
  </ol>
</section>

<section class="content" style="min-height: 550px">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">View Category Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Category</label>
              <?php echo $category->namaCategory; ?>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Created By</label>
                <?php echo $category->createdBy; ?>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Upload Image</label><br>
              <img width="200px" height="200px" src='<?php
                        $img = $category->imageCategory;
                        if ($img != '') {
                          echo base_url().'uploads/origin/category/'.$category->imageCategory;
                        } else {
                          echo base_url().'uploads/origin/default.png';
                        }
                        ?>'>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url()?>category" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
