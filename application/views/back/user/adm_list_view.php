<section class="content-header">
  <h1>
    User Management
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-users"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">User</li>
  </ol>
</section>

<section class="content" style="min-height: 490px">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">ADD DATA</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="datatable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Password</th>
                <th>Role</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($admins as $row){
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->username; ?></td>
                <td><?php echo $row->password; ?></td>
                <td><?php echo $row->role; ?></td>
                <td><?php echo $row->created_at; ?></td>
                <td><?php echo $row->updated_at; ?></td>
                <td><a href="<?php echo base_url();?>admin/form_edit/<?php echo $row->id; ?>" class="fa fa-edit">|
                    <a href="<?php echo base_url();?>admin/del_adminby/<?php echo $row->id; ?>" class="fa fa-trash-o" onclick="return confirm('Apakah anda yakin?')">|
                    <a href="<?php echo base_url();?>admin/get_admin/<?php echo $row->id; ?>" class="fa fa-eye"></a></td>
              </tr>
              <?php
            }
            ?>
              <!-- <a href="#" >Download Data</a> -->
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->




<!-- Pop Up Menu -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Admin Users</h4>
      </div>
      <form role="form" id="form-user" action="<?php echo site_url('admin/add_admin') ?>" method="post">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
              <label>Role :</label>
              <select class="form-control " name="role" id="role" required>
                  <option value="">Pilih -</option>
                  <option value="1">Admin</option>
                  <option value="2">User</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
