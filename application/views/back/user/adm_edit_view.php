<section class="content-header">
  <h1>
    Edit Users
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-users"></i> Provider</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content" style="min-height: 490px">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <!-- form start -->
        <form role="form" action="<?php echo site_url('admin/edit_admin/'.$admins->id) ?>" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" name="username" class="form-control" id="exampleInputEmail1" value="<?php echo $admins->username; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="<?php echo $admins->password; ?>">
            </div>
            <div class="form-group">
              <div class="col-md-2">ROLE:</div>
              <div class="col-md-10">
                <select class="form-control " name="role" id="role" required>
                    <option value=""><?php $admins->role;
                                      if ($admins == '1') {
                                        echo 'Admin';
                                      } else {
                                        echo "User";
                                      }
                                    ?>
                    </option>
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                </select>
              </div>
            </br>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $admins->id; ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>admin" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
