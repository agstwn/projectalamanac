<section class="content-header">
  <h1>
    View User
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-users"></i> User </a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Detail</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-hospital-o"></i>
          <h3 class="box-title"><?php echo $useradmin->username; ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>No</dt>
            <dd><?php echo $useradmin->id; ?></dd>
            <dt>Username</dt>
            <dd><?php echo $useradmin->username; ?></dd>
            <dt>Password</dt>
            <dd><?php echo $useradmin->password; ?></dd>
            <dt>Privilage</dt>
            <dd><?php if($useradmin->role == 1){echo "Admin";}else{echo "User";} ?></dd>
          </dl>
        </div>
        <form role="form">
          <div class="box-footer">
            <a href="<?php echo base_url()?>admin" class="btn btn-danger">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
