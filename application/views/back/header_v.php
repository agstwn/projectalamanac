<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

  </head>

  <body class="skin-red">
    <div class="wrapper">
      <header class="main-header">
        <a href="<?php echo base_url();?>admin" class="logo"><b>Admin</b></a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>assets/dist/img/admin.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs">Hi, <?php $data = $this->session->userdata('logged_in'); echo $data['username'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo base_url();?>assets/dist/img/admin.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php $data = $this->session->userdata('logged_in'); echo $data['username'];?> -
                      <?php $data = $this->session->userdata('logged_in');
                            if ($data['privilage'] == '1') {
                              echo 'Admin';
                            } else {
                              echo 'Not Admin';
                            }?>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="<?php echo base_url();?>logout" class="btn btn-default btn-flat">Logout</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url();?>assets/dist/img/admin.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <?php $data = $this->session->userdata('logged_in'); echo '<p>'.$data['username'].'</p>';?>
              <small>
                <?php $data = $this->session->userdata('logged_in');
                      if ($data['privilage'] == '1') {
                        echo 'Admin';
                      } else {
                        echo 'Not Admin';
                      }?>
              </small>
            </div>
          </div>
          <ul class="sidebar-menu" id="sidebar">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Users Management</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <?php $data = $this->session->userdata('logged_in');
              if($data['privilage'] == '1') { ?>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>admin"><i class="fa fa-user"></i>Data Admin</a></li>
              </ul>
              <?php }  else { ?>
                <ul class="treeview-menu hidden"></ul>
                <?php } ?>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-user"></i>Tittle</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Contents Management</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>content"><i class="fa fa-chevron-right"></i>Content</a></li>
                <li><a href="<?php echo base_url(); ?>about/aboutbackend"><i class="fa fa-chevron-right"></i>About</a></li>
                <li><a href="<?php echo base_url(); ?>contact/contactbackend"><i class="fa fa-chevron-right"></i>Contact</a></li>
                <li><a href="<?php echo base_url(); ?>category"><i class="fa fa-chevron-right"></i>Category</a></li>
              </ul>
            </li>
          </ul>
        </section>
      </aside>
      <div class="content-wrapper">
