<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('contact_m');
		$this->load->model('content_m');
		$this->load->helper('date');
		$this->load->helper('form');
	}

	public function index()
	{
		$data['kontak'] = $this->contact_m->getallcontactfrontend();
		$data['categories'] = $this->content_m->get_categories();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/contact/contact_v',$data);
		$this->load->view('front/footer_v');
	}



	/*Backend Controller For About*/
	public function contactbackend()
	{
		if($this->session->userdata('logged_in')){
			$data['contact'] = $this->contact_m->getallcontactbackend();
			$this->load->view('back/header_v');
			$this->load->view('back/contact/contact_list', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}



	public function contactaddview()
	{
		if($this->session->userdata('logged_in')){
			$this->load->view('back/header_v');
			$this->load->view('back/contact/contact_add');
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function addingcontact()
	{
		if(!$this->input->post()) show_404();

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		$data = array(
					'judul'			 => $this->input->post('judul'),
					'alamat'		 => $this->input->post('alamat'),
					'googlemap'	 => $this->input->post('googlemap'),
					'hp'			 	 => $this->input->post('hp'),
					'telp'			 => $this->input->post('telp'),
					'email'			 => $this->input->post('email'),
					'createdBy'	 => $createdby
		);

		$this->contact_m->add_contact($data);
		redirect('contact/contactbackend');
	}



	public function viewcontact($id)
	{

		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['kontak'] = $this->contact_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/contact/contact_view', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}



	public function delcontact($id)
	{
		if($this->session->userdata('logged_in')){
			$this->contact_m->deletingcontact($id);
			redirect('contact/contactbackend');
		}else{
			redirect('login');
		}
	}



	public function vieweditcontact($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['editkontak'] = $this->contact_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/contact/contact_edit', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function editcontact()
	{
		if(!$id = $this->input->post('id')) show_404();

		date_default_timezone_set('Asia/Jakarta');
		$datestring ="%Y-%m-%d %H:%i:%s";
		$time       =time();
		$datetime   =mdate($datestring, $time);

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		$data = array(
			'judul'			 => $this->input->post('judul'),
			'alamat'		 => $this->input->post('alamat'),
			'googlemap'	 => $this->input->post('googlemap'),
			'hp'			 	 => $this->input->post('hp'),
			'telp'			 => $this->input->post('telp'),
			'email'			 => $this->input->post('email'),
			'createdBy'	 => $createdby,
			'updated_at' => $datetime
		);

		$this->contact_m->update_contact($id, $data);
		redirect('contact/contactbackend');
	}





}
