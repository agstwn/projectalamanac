<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();

	$this->load->model('users_m');
	$this->load->helper('date');
	}



	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['admins'] = $this->users_m->get_alladmin();

			$this->load->view('back/header_v');
			$this->load->view('back/user/adm_list_view', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function add_admin()
	{
		$data = array (
					'username' 	 => $this->input->post('username'),
					'password' 	 => md5($this->input->post('password')),
					'role'   	 	 => $this->input->post('role')
					);

		$this->users_m->add_usermanage($data);
		redirect('admin/index');
	}



	public function get_admin($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();

			$data['useradmin'] = $this->users_m->get_adminby($id);
			$this->load->view('back/header_v');
			$this->load->view('admin/user/adm_view', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}



	public function form_edit($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();

			$data['admins'] = $this->users_m->get_adminby($id);
			$this->load->view('back/header_v');
			$this->load->view('back/user/adm_edit_view', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function edit_admin()
	{
		if(!$id = $this->input->post('id')) show_404();

		date_default_timezone_set('Asia/Jakarta');
		$datestring ="%Y-%m-%d %H:%i:%s";
		$time       =time();
		$datetime   =mdate($datestring, $time);

		$data = array(
					'username' 	 => $this->input->post('username'),
					'password' 	 => md5($this->input->post('password')),
					'role'	   	 => $this->input->post('role'),
					'updated_at' => $datetime
			);

		$this->users_m->update_admin($id, $data);
		redirect('admin/index');

	}



	public function del_adminby($id)
	{
		$this->users_m->del_adminby($id);
		redirect('admin/index');
	}

}
