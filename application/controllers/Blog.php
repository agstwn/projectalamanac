<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('content_m');
		$this->load->model('category_m');
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('pagination');
	}

	public function index()
	{
		$data['categories'] = $this->content_m->get_categories();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/blog/viewmore_v');
		$this->load->view('front/footer_v');
	}



	public function read_more()
	{
		$data['categories'] = $this->content_m->get_categories();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/blog/readmore_v');
		$this->load->view('front/footer_v');
	}



	public function viewmore()
	{
		$rslt = $this->content_m->get_viewmore()->num_rows();
		$data = $this->paging('blog/viewmore/',$rslt,'content_m','get_allcontentstartandlimit');
		$data['recentcontent'] = $this->content_m->get_recentcontent();
		$data['categories'] = $this->content_m->get_categories();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/viewmore/viewmore_v',$data);
		$this->load->view('front/footer_v');
	}



	public function paging($burl,$rslt,$model,$list,$type_id=null)
	{
		$config = array();
		$config['base_url']		     = base_url().$burl;
		$config['total_rows']	     = $rslt;
		$config["per_page"]		     = 6;
		$config["uri_segment"]	   = 3;
		$config['first_link']	     = 'First';
		$config['last_link']	     = 'Last';
		$config['next_link']	     = 'Next';
		$config['prev_link']	     = 'Prev';
		$config['next_tag_open']   = '<li>';
		$config['next_tag_close']  = '</li>';
		$config['prev_tag_open']   = '<li>';
		$config['prev_tag_close']  = '</li>';
		$config['num_tag_open']    = '<li>';
		$config['num_tag_close']   = '</li>';
		$config['first_tag_open']  = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open']   = '<li>';
		$config['last_tag_close']  = '</li>';
		$config['cur_tag_open']	   = '<li class="active"><a href="#">';
		$config['cur_tag_close']   = '</a></li>';

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data['halaman'] = $this->pagination->create_links();
		$data['view']    = $this->$model->$list($config["per_page"], $page, $type_id=null);
		// print_r($data); exit();
		$data['uri']     = $page;
		return $data;
	}


}
