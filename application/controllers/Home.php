<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('content_m');
		$this->load->model('category_m');
		$this->load->helper('date');
		$this->load->helper('form');
	}



	public function index()
	{
		$data['categories'] = $this->content_m->get_categories();
		$data['homecontact'] = $this->content_m->get_allcontentlimit();
		$data['contentech'] = $this->content_m->get_allcontentech();
		$data['lifestyle'] = $this->content_m->get_allcontentlifestyle();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/home_v',$data);
		$this->load->view('front/footer_v');
	}


}
