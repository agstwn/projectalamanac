<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verifylogin extends CI_Controller {

	public function __construct() {
				parent::__construct();
				$this->load->model('login_m','',TRUE);
				$this->load->library('session');
		}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('back/login_v');
		} else {
			$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
				);

			$result = $this->login_m->getadmin($data);
			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array( 'username' => $row->username, 'privilage' => $row->role );
					$this->session->set_userdata('logged_in', $sess_array);
				}
				redirect('admin');
			} else {
				$data = array('error_message' => 'Invalid Username or Password');
				$this->load->view('back/login_v', $data);
			}
		}
	}

}
