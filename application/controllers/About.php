<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('about_m');
		$this->load->model('content_m');
		$this->load->helper('date');
		$this->load->helper('form');
	}

	public function index()
	{
		$data['konten'] = $this->about_m->getallcontentfrontend();
		$data['categories'] = $this->content_m->get_categories();
		$this->load->view('front/header_v',$data);
		$this->load->view('front/about/about_v', $data);
		$this->load->view('front/footer_v');
	}



	/*Backend Controller For About*/
	public function aboutbackend()
	{
		if($this->session->userdata('logged_in')){
			$data['about'] = $this->about_m->getallcontentbackend();
			$this->load->view('back/header_v');
			$this->load->view('back/about/about_list', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}



	public function aboutaddview()
	{
		if($this->session->userdata('logged_in')){
			$this->load->view('back/header_v');
			$this->load->view('back/about/about_add');
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function addingabout()
	{
		if(!$this->input->post()) show_404();

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path 					 = './uploads/origin/about';
		$thumbs_path 						 = './uploads/thumb/about';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);

		$this->load->library('upload', $config);
		$this->upload->do_upload('img');
		$image_data = $this->upload->data(); //upload the image

		$image['img'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' 	 => $image_data['full_path'],
			'new_image' 		 => $thumbs_path,
			'maintain_ratio' => true,
			'width' 				 => 100,
			'height'	 			 => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'judul'			 => $this->input->post('judul'),
					'isi'				 => $this->input->post('isi'),
					'img'				 => $image['img'],
					'createdBy'	 => $createdby
		);

		$this->about_m->add_about($data);
		redirect('about/aboutbackend');
	}



	public function viewabout($id)
	{

		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['konten'] = $this->about_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/about/about_view', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}

	public function delabout($id)
	{
		if($this->session->userdata('logged_in')){
			$this->about_m->deletingabout($id);
			redirect('about/aboutbackend');
		}else{
			redirect('login');
		}
	}



	public function vieweditabout($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['editkonten'] = $this->about_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/about/about_edit', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}



	public function editabout()
	{
		if(!$id = $this->input->post('id')) show_404();

		date_default_timezone_set('Asia/Jakarta');
		$datestring ="%Y-%m-%d %H:%i:%s";
		$time       =time();
		$datetime   =mdate($datestring, $time);

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path = './uploads/origin/about';
		$thumbs_path 	 = './uploads/thumb/about';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);
		// $config['file_name'] = $dp;
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto2');
		$image_data = $this->upload->data(); //upload the image

		$image['foto2'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $thumbs_path,
			'maintain_ratio' => true,
			'width' => 100,
			'height' => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'judul'			 	=> $this->input->post('judul'),
					'isi'				 	=> $this->input->post('isi'),
					'createdBy'	 	=> $createdby,
					'img'					=> $image['foto2'],
					'updated_at' 	=> $datetime
			);

		$data2 = array(
					'judul'			 	=> $this->input->post('judul'),
					'isi'				 	=> $this->input->post('isi'),
					'createdBy'	 	=> $createdby,
					'img'					=> $this->input->post('images'),
					'updated_at' 	=> $datetime
			);

		if($data['img'] == FALSE) {
			$this->about_m->update_about($id, $data2);
			redirect('about/aboutbackend');
			} else {
			$this->about_m->update_about($id, $data);
			redirect('about/aboutbackend');
			}
		}


}
