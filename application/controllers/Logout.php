<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// session_start();

class Logout extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index()
	{
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		session_destroy();
		redirect('login', 'refresh');
	}

}
