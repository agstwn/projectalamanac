<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public function __construct() {
	parent::__construct();

	$this->load->model('category_m');
	$this->load->helper('date');
	$this->load->helper('form');

	}

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['content'] = $this->category_m->get_allcategory();
			$this->load->view('back/header_v');
			$this->load->view('back/category/category_list', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}

	public function contentaddview()
	{
		if($this->session->userdata('logged_in')){
			$this->load->view('back/header_v');
			$this->load->view('back/category/category_add');
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}

	public function addingcategory()
	{
		if(!$this->input->post()) show_404();

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path 					 = './uploads/origin/category';
		$thumbs_path 						 = './uploads/thumb/category';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);
		// $config['file_name'] = $dp;
		$this->load->library('upload', $config);
		$this->upload->do_upload('imageCategory');
		$image_data = $this->upload->data(); //upload the image

		$image['imageCategory'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' 	 => $image_data['full_path'],
			'new_image' 		 => $thumbs_path,
			'maintain_ratio' => true,
			'width' 				 => 100,
			'height'	 			 => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'namaCategory'	=> $this->input->post('namaCategory'),
					'createdBy'			=> $createdby,
					'imageCategory'	=> $image['imageCategory']
		);

		$this->category_m->add_category($data);
		redirect('category/index');
	}

	public function viewcategory($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['category'] = $this->category_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/category/category_view', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}

	public function del_productinfo($id)
	{
		if($this->session->userdata('logged_in')){
			$this->category_m->del_category($id);
			redirect('category/index');
		}else{
			redirect('login');
		}
	}

	public function form_editcategory($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['editkategori'] = $this->category_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/category/category_edit', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}

	public function edit_category()
	{
		if(!$id = $this->input->post('id')) show_404();

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		date_default_timezone_set('Asia/Jakarta');
		$datestring ="%Y-%m-%d %H:%i:%s";
		$time       =time();
		$datetime   =mdate($datestring, $time);

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path = './uploads/origin/category';
		$thumbs_path 	 = './uploads/thumb/category';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);
		// $config['file_name'] = $dp;
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto2');
		$image_data = $this->upload->data(); //upload the image

		$image['foto2'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $thumbs_path,
			'maintain_ratio' => true,
			'width' => 100,
			'height' => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'namaCategory'	=> $this->input->post('namaCategory'),
					'createdBy'			=> $createdby,
					'imageCategory'	=> $image['foto2'],
					'updated_at' 		=> $datetime
			);

		$data2 = array(
					'namaCategory'	=> $this->input->post('namaCategory'),
					'createdBy'			=> $createdby,
					'imageCategory'	=> $this->input->post('images'),
					'updated_at' 		=> $datetime
			);

		if($data['imageCategory'] == FALSE) {
			$this->category_m->update_category($id, $data2);
			redirect('category/index');
			} else {
			$this->category_m->update_category($id, $data);
			redirect('category/index');
			}
		}
}
