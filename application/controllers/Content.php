<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {
	public function __construct() {
	parent::__construct();

	$this->load->model('content_m');
	$this->load->model('category_m');
	$this->load->helper('date');
	$this->load->helper('form');

	}

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['content'] = $this->content_m->get_allcontent();
			$this->load->view('back/header_v');
			$this->load->view('back/content/content_list', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}

	public function contentaddview()
	{
		if($this->session->userdata('logged_in')){
			$type['category'] = $this->category_m->get_allcategory();
			$this->load->view('back/header_v');
			$this->load->view('back/content/content_add',$type);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}

	public function addingcontent()
	{
		if(!$this->input->post()) show_404();

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path 					 = './uploads/origin/content';
		$thumbs_path 						 = './uploads/thumb/content';
		$ext										 = 'content_';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);

		$this->load->library('upload', $config);
		$this->upload->do_upload('img');
		$image_data = $this->upload->data(); //upload the image

		$image['img'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' 	 => $image_data['full_path'],
			'new_image' 		 => $thumbs_path,
			'maintain_ratio' => true,
			'width' 				 => 100,
			'height'	 			 => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'judul'			 => $this->input->post('judul'),
					'isi'				 => $this->input->post('isi'),
					'img'				 => $image['img'],
					'video'			 => $this->input->post('video'),
				  'category' 	 => $this->input->post('category'),
					'createdBy'	 => $createdby
		);

		$this->content_m->add_content($data);
		redirect('content/index');
	}

	public function viewcontent($id)
	{

		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['konten'] = $this->content_m->get_by_id($id);
			$this->load->view('back/header_v');
			$this->load->view('back/content/content_view', $data);
			$this->load->view('back/footer_v');
		}else {
			redirect('login');
		}
	}

	public function delcontent($id)
	{
		if($this->session->userdata('logged_in')){
			$this->content_m->deletingcontent($id);
			redirect('content/index');
		}else{
			redirect('login');
		}
	}

	public function vieweditcontent($id)
	{
		if($this->session->userdata('logged_in')){
			if(!$id) show_404();
			$data['editkonten'] = $this->content_m->get_by_id($id);
			$data['category'] = $this->category_m->get_allcategory();
			$this->load->view('back/header_v');
			$this->load->view('back/content/content_edit', $data);
			$this->load->view('back/footer_v');
		}else{
			redirect('login');
		}
	}

	public function editcontent()
	{
		if(!$id = $this->input->post('id')) show_404();

		date_default_timezone_set('Asia/Jakarta');
		$datestring ="%Y-%m-%d %H:%i:%s";
		$time       =time();
		$datetime   =mdate($datestring, $time);

		$data 		 = $this->session->userdata('logged_in');
		$createdby = $data['username'];

		/*Upload Image*/
		$config['image_library'] = 'gd2';
		$original_path = './uploads/origin/content';
		$thumbs_path 	 = './uploads/thumb/content';
		$this->load->library('image_lib', $config);

		$config = array(
			'allowed_types' => 'jpg|jpeg|png', //only accept these file types
			'max_size' 			=> 2048, //1MB max
			'upload_path' 	=> $original_path //upload directory
		);
		// $config['file_name'] = $dp;
		$this->load->library('upload', $config);
		$this->upload->do_upload('foto2');
		$image_data = $this->upload->data(); //upload the image

		$image['foto2'] = $image_data['file_name'];

		// for the Thumbnail image
		$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $thumbs_path,
			'maintain_ratio' => true,
			'width' => 100,
			'height' => 100
		);
		//here is the second thumbnail, notice the call for the initialize() function again
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		/*Upload Image*/

		$data = array(
					'judul'			 	=> $this->input->post('judul'),
					'isi'				 	=> $this->input->post('isi'),
					'video'			 	=> $this->input->post('video'),
					'category' 	 	=> $this->input->post('category'),
					'createdBy'	 	=> $createdby,
					'img'					=> $image['foto2'],
					'updated_at' 	=> $datetime
			);

		$data2 = array(
					'judul'			 	=> $this->input->post('judul'),
					'isi'				 	=> $this->input->post('isi'),
					'video'			 	=> $this->input->post('video'),
					'category' 	 	=> $this->input->post('category'),
					'createdBy'	 	=> $createdby,
					'img'					=> $this->input->post('images'),
					'updated_at' 	=> $datetime
			);

		if($data['img'] == FALSE) {
			$this->content_m->update_productinfo($id, $data2);
			redirect('content/index');
			} else {
			$this->content_m->update_productinfo($id, $data);
			redirect('content/index');
			}
		}
}
